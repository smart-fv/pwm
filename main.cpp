#include "mbed.h"
#include "Slave.h"
#include <chrono>
// Da modificare quando lo si collega per il test
uint16_t T = 0;
Slave slave(D8, D2, 9600, 2, &T);

// Output PWM
InterruptIn zerocross(D7); // Interrupt (segnale con cui sincronizzarsi)
DigitalOut TriacGate(D6); // Pin per l'ouput PWM
Timeout triacTriggerOn;
const int powerlinefrequency = 50;

// Porta uscita a 0 una volta scaduto il tempo prefissato
void triggerOff() {
    TriacGate = 0;
}

// Routine compiuta quando vione attivato l'interrupt
void zeroCrossingInterrupt() {
    if (T == 0) {
        return;
    }     
    TriacGate = 1;
    
    triacTriggerOn.attach(&triggerOff, std::chrono::microseconds((T * 1000) / 
        (2*powerlinefrequency))); 
                                                                        
}

void readVal() {
    while (true) {
        slave.cycle();
    }
}

int main()
{    
    Thread t1;
    t1.start(readVal);
    zerocross.mode(PullUp);
    thread_sleep_for(200);
    zerocross.rise(&zeroCrossingInterrupt);
    while (true) {        
    }
}


